import 'dart:collection';

class RequestContext {
  RequestContext() {
    _mapForKey(null);
  }

  final Map<String, Map<Type, dynamic>> _data = HashMap();

  void clear() {
    _data.clear();
  }

  T dataFor<T>({String key}) {
    return _mapForKey(key)[T] as T;
  }

  bool addDataFor<T>(T val, {String key}) {
    return _mapForKey(key).putIfAbsent(T, () => val) == val;
  }

  void replaceDataFor<T>(T val, {String key}) {
    _mapForKey(key)[T] = val;
  }

  bool containsDataFor<T>({String key}) {
    return _mapForKey(key).containsKey(T);
  }

  Map<Type, dynamic> _mapForKey(String key) {
    return _data.putIfAbsent(key, () => {});
  }
}
